DLSConfig = {}
DLSConfig.DevMode = false


-- Do not remove this
-- Created by: ♕ Dilly ✄
-- Steamid: http://steamcommunity.com/id/DillyRektChu/

DLSConfig.PriceForTicket = 100

DLSConfig.RewardMatch1 = 1000 -- The amount of money won for matching 1 number
DLSConfig.RewardMatch2 = 5000 -- The amount of money won for matching 2 numbers
DLSConfig.RewardMatch3 = 10000 -- The amount of money won for matching 3 numbers

DLSConfig.CoolDown = 120 -- The amount of time (in sec) a player has to wait to buy another ticket

DLSConfig.AutoBanExploiters = true -- THIS ONLY WORKS FOR ULX IF YOU DO NOT USE ULX DO NOT USE THIS
-- If a player tries to use exploits with this addon he/she will be auto banned (ulx banid <players id> Attempting to use exploits on the lottery addon)